<?php

class sec_context_condition extends context_condition {
	public function condition_values() {
		return site_entity_options_get();
	}

	public function options_form($ctx) {
		$v = $this->fetch_from_context($ctx);

		$neg = 0;
		if(isset($v['options']['negate'])) {
			$neg = $v['options']['negate'];
		}

		$form['negate'] = array(
			'#type'          => 'checkbox',
			'#title'         => t('Negate'),
			'#default_value' => $neg,
		);

		return $form;
	}

	public function execute($id) {
		if(!$this->condition_used()) {
			return;
		}

		foreach($this->get_contexts() as $ctx) {
			if(empty($ctx->conditions[$this->plugin])) {
				continue;
			}
			$p = $ctx->conditions[$this->plugin];
			$sites = $p['values'];
			$neg = $p['options']['negate'];

			$matched = in_array($id, $sites);
			if(($matched && !$neg) || (!$matched && $neg)) {
				$this->condition_met($ctx);
			}
		}
	}
}
