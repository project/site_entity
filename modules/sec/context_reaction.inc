<?php

class sec_context_reaction extends context_reaction {
	public function options_form($ctx) {
		$v = $this->fetch_from_context($ctx);
		$form = array('#tree' => TRUE);

		$def = 0;
		if(isset($v['site'])) {
			$def = $v['site'];
		}

		$form['site'] = array(
			'#type'          => 'select',
			'#title'         => t('Set site to'),
			'#multiple'      => FALSE,
			'#required'      => TRUE,
			'#options'       => site_entity_options_get(),
			'#default_value' => $def,
		);

		return $form;
	}

	public function execute() {
		if(is_site_entity_selected()) {
			watchdog('sec', 'set current site entity reaction attempting to run after cse chosen', WATCHDOG_ERROR);
			return;
		}

		foreach($this->get_contexts() as $ctx) {
			if(empty($ctx->reactions[$this->plugin])) {
				continue;
			}
			$p = $ctx->reactions[$this->plugin];

			return $p['site'];
		}
	}
}
