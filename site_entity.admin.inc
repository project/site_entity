<?php

function site_entity_form($form, &$state, $se = NULL) {
	$form['name'] = array(
		'#type'          => 'textfield',
		'#title'         => t('Name'),
		'#size'          => 30,
		'#maxlength'     => 255,
		'#required'      => TRUE,
		'#default_value' => isset($se->name) ? $se->name : '',
	);

	$form['machine_name'] = array(
		'#type'          => 'machine_name',
		'#title'         => t('Machine name'),
		'#size'          => 15,
		'#maxlength'     => 255,
		'#default_value' => isset($se->machine_name) ? $se->machine_name : '',
		'#machine_name'  => array(
			'source' => array('name'),
			'exists' => '_site_entity_machine_name_exists',
		),
	);

	field_attach_form('site_entity', $se, $form, $state);

	$form['submit'] = array(
		'#type'   => 'submit',
		'#value'  => t('Save'),
		'#weight' => 500,
	);

	return $form;
}

function _site_entity_machine_name_exists($value) {
	$q = db_query('select 1 from {site_entity} where machine_name=:name', array(':name' => $value));
	return $q->fetchColumn(0);
}

function site_entity_form_submit($form, &$state) {
	$se = entity_ui_form_submit_build_entity($form, $state);
	$se->save();
	$state['redirect'] = 'admin/structure/site-entity';
}

