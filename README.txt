-- SUMMARY --

Site Entity makes a Drupal site fieldable.

Site Entity creates an entity type and a default entity which is loaded during hook_init.

All view modes (except tokens) on this entity type are exposed as blocks.
To create additional blocks use Display Suite ( https://www.drupal.org/project/ds ) or Entity View Modes ( https://www.drupal.org/project/entity_view_mode ) to add new view modes.

A simple use case

The problem

You're creating a site for a local store. You need a block in the footer that lists the phone number. The phone number may change in the future. You want the site owners to be able to edit the phone number, but don't want to expose all the functionality of blocks or disturb the complex formatting the theme requires to render the number just so.

The solution

Add a telephone field to the site entity. Give the owners permission to edit the site entity. Create a footer view mode for site entities. Use manage display to set it up just how it needs to be set up.

Now the owners can easily change the phone number using the same editing interface they would for any entity without having to muck around with blocks.

A more advanced use case

The problem

You have created a sophisticated social networking site for a large organization with many staff. There are many events created by the site that staff require e-mail notifications for, such as when users pay for subscriptions, a subscription will expire, a user requires approval to upgrade an account, etc., etc. Not all staff need to receive all notifications and there are too many notifications to just send all of them to everyone and let them handle only the ones they need to handle. Which staff member receives which notifications can change and will change often.

The solution

Create a Views Reference Filter ( https://www.drupal.org/project/entityreference_filter ) for users with the staff role.

For each notification type, add an unlimited Entity Reference field ( https://www.drupal.org/project/entityreference ) to the site entity.

For each notification rule, use Site Entity Rules (see below) to load the default site entity, get the list of users to send the notification to, and loop through the values to send the e-mails.

Now the staff can easily specify who receives which notifications—so easily that they can temporarily change who receives which notifications for temporary events such as a staff member being out sick for the day.


Included Submodules

Site Entity Settings (ses) - override site variables based on the values of Site Entity fields.

Site Entity Context (sec) - Integration with the Context module ( https://www.drupal.org/project/context ). Allows context to choose or react to the current site entity.

Site Entity Rules (ser) - Integration with the Rules module ( https://www.drupal.org/project/rules ). This provides
* Events
** When choosing the current site entity
** After the current site entity has been chosen
* Conditions
** The current site entity has been chosen
** The current site entity is the default site entity
* Actions
** Set current site entity (it is an error to invoke this for any event other than "When choosing the current site entity)
** Load current site entity (This operation is undefined if the current site entity has not been chosen)
** Load default site entity	(This is always safe to call)

Using only the provided site entity works 100%, however while the creation of additional site entities is supported, but dynamically specifying which is the "current" site entity, as provided by the above modules, is an experimental feature and may require tweaking to work 100%. Specifically, it does work, but ensuring that the proper site entity has been chosen can be unintuitive and selection may need to be moved to hook_boot or it may just need have its module weight altered so that it always runs first. Help wanted!

Desired submodules

These are ideas for submodules that would be most welcome by contributors. (Though feel free to surprise and delight with something not listed below!)

Views integration. A way to either always include the fields of the current site entity in any view, or some way to add a fake relationship to include the views. Bonus points for a global handler to render the current site entity with a given view mode.

Domain Access ( https://www.drupal.org/project/domain ) integration. It would be handy to choose the current site entity based on the current domain.

An equivalent of or extension to Entityreference Current ( https://www.drupal.org/project/entityreference_current ) that records the current site entity. This would be handy for multi-store Commerce ( https://www.drupal.org/project/commerce ) sites and Entityforms ( https://www.drupal.org/project/entityform ).

Terminology, lifecycle, and API

This section is unnecessary unless you want to understand how this module works or wish to use it for something very advanced or not enabled by the included modules.

During hook_init Site Entity invokes hook_site_entity_choose with the id of the default site entity. If any modules implementing this hook return a truthy value, the last such value is used to determine which site entity to choose.

After this the site entity is selected, hook_site_entity_chosen is invoked, allowing any modules to react to this event.

To query whether the site entity was selected, call is_site_entity_selected().

The default site entity is the site entity created when this module is installed and will always be loaded the current site entity if no modules intervene using hook_site_entity_choose. The default site entity is always site entity 1, unless overridden by setting the site_entity_default variable.

The current site entity is the site entity that was chosen by hook_site_entity_choose. It may be queried with site_entity_current().

Otherwise site entities are just regular entities and can be used as any other entity can be used.

For a full description of the module, visit the project page:
  https://drupal.org/projects/site_entity

To submit bug reports and feature suggestions, or to track changes:
  https://drupal.org/project/issues/site_entity

-- REQUIREMENTS --

Entity ( https://drupal.org/project/entity )


-- INSTALLATION --

Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

-- CONTACT --

Current maintainers:
* James Frasche (soapboxcicero) - https://drupal.org/user/2111168

This project has been sponsored by:
* MorseMedia
  Visit https://morsemedia.net
